import {utils} from "./utils";


export let WS = {

    msg: JSON.stringify({
        event: 'subscribe',
        channel: 'ticker',
        symbol: 'tBTCUSD'
    }),

    connect: function (url) {
        return new Promise((resolve, reject) => {
            var ws = new WebSocket(url);
            ws.onopen = function () {
                console.log(ws)
                //TRADES
                ws.send(WS.msg);
                resolve(ws);

            };
            ws.onerror = function (err) {
                reject(err);
            };

        });
    },

    onMessage: function (ws) {
        ws.onmessage = function (msg) {     // callback on message receipt
            let data = msg.data
            let response = JSON.parse(data);
            console.log(response)
            utils.eventSwitch(response)

            var chatitem =
                "<div class='chattime'>" + response[1][0] + "</div>";

            $(".chattable").prepend(
                $(chatitem).hide().fadeTo('fast', 1)
            );


            // console.log(eventSwitch(response))
        }
    }
}
